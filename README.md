
# Polls project 

## Setup
The first thing to do is to clone the repository:
```sh
$ git clone https://gitlab.com/longngvan.it/HelloWorld_app.git
$ cd HelloWorld_app
```
Create a virtual environment to install dependencies and activate it:
```sh
$ python3 -m venv env
$ source env/bin/activate
```


## Install poetry package 
A package help install package in project.Install with Ububtu:
```shell script
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
pip install poetry
```

Then install the dependencies:
```shell script
(env)$ poetry install
```

if install `psycopg2` error. You can install `psycopg2-binary` instead

# Setting up Database in Ubuntu
if you are using Linux OS, PostgreSQL support we don't need password,user,host. We only use name database to know project storage.
It's very benefit when we forget the Credential database. When we install PostgreSQL, we need:
```shell script
$ sudo -u postgres createuser -s $USER
$ sudo -u postgres createdb my_project -O $USER
```
the first script show us create user similar user os with connect Unit socket (-s role superuser).
The second script, we create database with name `my_project` and set `-O` Owner database help full permission with it.
Project use PostgreSQl and Credential database save in `settings_default.yaml`.
Edit information in it:
```shell script
  # Configure database
  DB_NAME: PollDB
```
With another os, we need configure full Credential database.
## Migrate project
You must migrate project in order to create Table in your Database:

```shell script
(env)$ python manage.py migrate
```

## Run project
```shell script
(env) $ python manage.py runserver
```
And navigate to `http://127.0.0.1:8000/polls`

## Description project
User can create the Polls in admin page and vote the answers to the question.

##  Deploy project with gunicorn and Nginx on Ubuntu
Install gunicorn in Ubuntu: 
```shell script
$ pip install gunicorn
```
First, you must configure in order to creating systemd Socket and Service Files for GuniCorn
```shell script
$ sudo nano /usr/local/lib/systemd/system/gunicorn.socket
```
Copy `deploy/gunicorn_socket` file to the path file above and save it.
Next , you set up `depploy/gunicorn_service` file to path file and save  below:
```shell script
$ sudo nano /usr/local/lib/systemd/system/gunicorn.service
```
Thirdly, when you configure gunicorn file .We can now start and enable Gunicorn socket.Scirpt follow:
```shell script
$ sudo systemctl start gunicorn.socket
$ sudo systemctl enable gunicorn.socket
```
If you want to test Socket activation.You can check this by typing:
```shell script
$ systemctl status gunicorn
```
And if you make change to `gunicorn.service` file, reload the daemon to reread the service definition and restart the Gunicorn process by typing:
```shell script
$ sudo systemctl daemon-reload
$ sudo systemctl restart gunicorn
```
When we set up gunicorn successfully.Next step, we install Nginx:
```shell script
$ sudo apt-get install nginx
```
Next, we configure Nginx to proxy pass to Gunicorn.We must start and open a new server block in Nginx's `sites-avaiable` dir:
```shell script
$ sudo nano /etc/nginx/sites-available/HelloWorld_app
```
Configure path file above with `deploy/Nginx_deployed` and save it.Now we can enable  the file by linking it to the `sites-enabled` directory:
```shell script
$ sudo ln -s /etc/nginx/sites-available/HelloWorld_app /etc/nginx/sites-enabled
```

Then, we restart Nginx and open port in order to access to the development server:
```shell script
$ sudo systemctl restart nginx
```
Finally, we should now be able to go to your  server’s domain or IP address to view your application.